public class SuperPersonClass {
    public static void main(String[] args) {
        Person person1 = new Person("Thạnh", " 148 Trần Khắc Chân P9 Q.PN ");
        Person person2 = new Person("Lê Thạnh ", "Q12");

        System.out.println( "person1: " + person1.toString() + "\n" +
                            "person2: " + person2.toString());

        Student student1 = new Student("Thạnh", " 148 Trần Khắc Chân P9 Q.PN ", "Developer", 1990, 2.0);
        Student student2 = new Student("Lê Thạnh", "Q12 ", "Sofware Engineer", 1990, 3.0);

        
        System.out.println( "student1: " + student1.toString() + "\n" +
                            "student2: " + student2.toString());

        Staff staff1 = new Staff("Thạnh", " 148 Trần Khắc Chân P9 Q.PN ", "IronHack", 3.0) ; 
        Staff staff2 = new Staff("Lê Thạnh", "Q12", "IronHack 120 Ngày", 4.0) ; 
        
        System.out.println(  "staff1: " + staff1.toString() + "\n" +
                             "staff2: " + staff2.toString());

    }
}
